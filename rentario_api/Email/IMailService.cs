﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rentario_api.Email
{
    public interface IMailService
    {
        Task SendEmailAsync(string emailAddress, string code);
    }
}
