﻿using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rentario_api.Email
{
    public class MailService: IMailService
    {
        private readonly SettingsMail _mailSettings;
        public MailService(IOptions<SettingsMail> mailSettings)
        {
            _mailSettings = mailSettings.Value;
        }

        public async Task SendEmailAsync(string emailAddress, string code)
        {
            try
            {
                MimeMessage email = new MimeMessage();
                email.Sender = MailboxAddress.Parse(_mailSettings.Mail);
                email.To.Add(MailboxAddress.Parse(emailAddress));
                email.Subject = "Recuperación de tu contraseña";
                BodyBuilder builder = new BodyBuilder();
                builder.TextBody = $"Utiliza este código para poder recuperar tu contraseña: {code}. Haz caso omiso a este correo si no has sido tú.";
                email.Body = builder.ToMessageBody();
                using var smtp = new SmtpClient();
                smtp.Connect(_mailSettings.Host, _mailSettings.Port, SecureSocketOptions.StartTls);
                smtp.Authenticate(_mailSettings.Mail, _mailSettings.Password);
                await smtp.SendAsync(email);
                smtp.Disconnect(true);
            }
            catch(Exception ex)
            {
                throw ex;
            }
            
        } 
    }
}
