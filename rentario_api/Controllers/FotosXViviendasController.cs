﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using rentario_api.Models;

namespace rentario_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FotosXViviendasController : ControllerBase
    {
        private readonly rentarioContext _context;

        public FotosXViviendasController(rentarioContext context)
        {
            _context = context;
        }

        // GET: api/FotosXViviendas
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IEnumerable<FotosXVivienda>>> GetFotosXVivienda()
        {
            return await _context.FotosXVivienda.ToListAsync();
        }

        // GET: api/FotosXViviendas/5
        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<FotosXVivienda>> GetFotosXVivienda(int id)
        {
            var fotosXVivienda = await _context.FotosXVivienda.FindAsync(id);

            if (fotosXVivienda == null)
            {
                return NotFound();
            }

            return fotosXVivienda;
        }

        // PUT: api/FotosXViviendas/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        [Authorize]
        public async Task<IActionResult> PutFotosXVivienda(int id, FotosXVivienda fotosXVivienda)
        {
            if (id != fotosXVivienda.Id)
            {
                return BadRequest();
            }

            _context.Entry(fotosXVivienda).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FotosXViviendaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/FotosXViviendas
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<FotosXVivienda>> PostFotosXVivienda(FotosXVivienda fotosXVivienda)
        {
            _context.FotosXVivienda.Add(fotosXVivienda);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFotosXVivienda", new { id = fotosXVivienda.Id }, fotosXVivienda);
        }

        // DELETE: api/FotosXViviendas/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<ActionResult<FotosXVivienda>> DeleteFotosXVivienda(int id)
        {
            var fotosXVivienda = await _context.FotosXVivienda.FindAsync(id);
            if (fotosXVivienda == null)
            {
                return NotFound();
            }

            _context.FotosXVivienda.Remove(fotosXVivienda);
            await _context.SaveChangesAsync();

            return fotosXVivienda;
        }

        private bool FotosXViviendaExists(int id)
        {
            return _context.FotosXVivienda.Any(e => e.Id == id);
        }
    }
}
