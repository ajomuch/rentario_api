﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using rentario_api.Models;

namespace rentario_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PuntuacionPorArrendatariosController : ControllerBase
    {
        private readonly rentarioContext _context;

        public PuntuacionPorArrendatariosController(rentarioContext context)
        {
            _context = context;
        }

        // GET: api/PuntuacionPorArrendatarios
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IEnumerable<PuntuacionPorArrendatario>>> GetPuntuacionPorArrendatario()
        {
            return await _context.PuntuacionPorArrendatario.ToListAsync();
        }

        // GET: api/PuntuacionPorArrendatarios/5
        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<PuntuacionPorArrendatario>> GetPuntuacionPorArrendatario(int id)
        {
            var puntuacionPorArrendatario = await _context.PuntuacionPorArrendatario.FindAsync(id);

            if (puntuacionPorArrendatario == null)
            {
                return NotFound();
            }

            return puntuacionPorArrendatario;
        }

        // PUT: api/PuntuacionPorArrendatarios/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        [Authorize]
        public async Task<IActionResult> PutPuntuacionPorArrendatario(int id, PuntuacionPorArrendatario puntuacionPorArrendatario)
        {
            if (id != puntuacionPorArrendatario.Id)
            {
                return BadRequest();
            }

            _context.Entry(puntuacionPorArrendatario).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PuntuacionPorArrendatarioExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/PuntuacionPorArrendatarios
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<PuntuacionPorArrendatario>> PostPuntuacionPorArrendatario(PuntuacionPorArrendatario puntuacionPorArrendatario)
        {
            _context.PuntuacionPorArrendatario.Add(puntuacionPorArrendatario);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPuntuacionPorArrendatario", new { id = puntuacionPorArrendatario.Id }, puntuacionPorArrendatario);
        }

        // DELETE: api/PuntuacionPorArrendatarios/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<ActionResult<PuntuacionPorArrendatario>> DeletePuntuacionPorArrendatario(int id)
        {
            var puntuacionPorArrendatario = await _context.PuntuacionPorArrendatario.FindAsync(id);
            if (puntuacionPorArrendatario == null)
            {
                return NotFound();
            }

            _context.PuntuacionPorArrendatario.Remove(puntuacionPorArrendatario);
            await _context.SaveChangesAsync();

            return puntuacionPorArrendatario;
        }

        private bool PuntuacionPorArrendatarioExists(int id)
        {
            return _context.PuntuacionPorArrendatario.Any(e => e.Id == id);
        }
    }
}
