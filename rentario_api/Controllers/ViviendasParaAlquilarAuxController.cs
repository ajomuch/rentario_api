﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using rentario_api.Models;
using rentario_api.Utils;

namespace rentario_api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ViviendasParaAlquilarAuxController : ControllerBase
    {
        private readonly rentarioContext _context;
        public ViviendasParaAlquilarAuxController(rentarioContext context)
        {
            _context = context;
        }

        [HttpGet]
        [ActionName("GetHouseByPage")]
        [Authorize]
        public IActionResult GetHouseByPage([FromQuery]int Page, int Size, int IdUsuario)
        {
            try
            {
                List<int> idsFavoritas = _context.Favoritas.Where(r => r.IdUsuario == IdUsuario).Select(r => r.IdVivienda).ToList();
                List<VwViviendasParaAlquilar> list = _context
                    .VwViviendasParaAlquilar
                    .OrderByDescending(r => r.FechaPublicacion)
                    .Skip((Page - 1) * Size)
                    .Take(Size).ToList();
                foreach(VwViviendasParaAlquilar home in list)
                {
                    home.Favorita = idsFavoritas.Contains((int)home.Id);
                    home.Fotos = _context.FotosXVivienda.Where(r => r.Id == home.Id).ToList();
                }
                return Ok(list);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet]
        [ActionName("GetHouseByPageFiltered")]
        [Authorize]
        public IActionResult GetHouseByPageFiltered([FromQuery]int Page, int Size, int IdUsuario, string filter)
        {
            try
            {
                FilterHouse filterHouse;
                if (!string.IsNullOrEmpty(filter))
                    Cast.TryCast(filter, out filterHouse);
                else
                    return BadRequest();
                if (filterHouse == null)
                    return BadRequest();

                List<int> idsFavoritas = _context.Favoritas.Where(r => r.IdUsuario == IdUsuario).Select(r => r.IdVivienda).ToList();
                IQueryable<VwViviendasParaAlquilar> query = _context.VwViviendasParaAlquilar;
                if (filterHouse.IdTipoVivienda.HasValue)
                {
                    query = query.Where(r => r.IdTipoVivienda == filterHouse.IdTipoVivienda);
                }
                if (!string.IsNullOrEmpty(filterHouse.Colonia))
                {
                    query = query.Where(r => r.Colonia.Contains(filterHouse.Colonia));
                }
                if (filterHouse.IdPais.HasValue)
                {
                    query = query.Where(r => r.IdPais == filterHouse.IdPais);
                }
                if (filterHouse.IdDivision.HasValue)
                {
                    query = query.Where(r => r.IdDivision == filterHouse.IdDivision);
                }
                if (filterHouse.IdSubdivision.HasValue)
                {
                    query = query.Where(r => r.IdSubdivision == filterHouse.IdSubdivision);
                }
                if (filterHouse.PrecioInicial.HasValue && filterHouse.PrecioInicial.Value > 0)
                {
                    query = query.Where(r => r.Precio >= filterHouse.PrecioInicial);
                }
                if (filterHouse.PrecioFinal.HasValue && filterHouse.PrecioFinal.Value > 0)
                {
                    query = query.Where(r => r.Precio <= filterHouse.PrecioFinal);
                }
                if (filterHouse.Puntuacion.HasValue && filterHouse.Puntuacion.Value > 0)
                {
                    double ceil = Math.Ceiling(filterHouse.Puntuacion.Value);
                    query = query.Where(r => (ceil - 1) <= r.PromedioPuntuacion && r.PromedioPuntuacion <= ceil);
                }
                if (filterHouse.Dormitorios.HasValue)
                {
                    query = query.Where(r => r.Dormitorios == filterHouse.Dormitorios);
                }
                query = query
                    .OrderByDescending(r => r.FechaPublicacion)
                    .Skip((Page - 1) * Size)
                    .Take(Size);
                List<VwViviendasParaAlquilar> list = query.ToList();
                foreach (VwViviendasParaAlquilar home in list)
                {
                    home.Favorita = idsFavoritas.Contains((int)home.Id);
                    home.Fotos = _context.FotosXVivienda.Where(r => r.Id == home.Id).ToList();
                }
                return Ok(list);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet]
        [ActionName("GetHouseByUser")]
        [Authorize]
        public IActionResult GetHouseByUser([FromQuery]int Page, int Size, int IdUsuario)
        {
            try
            {
                
                List<VwViviendasParaAlquilar> list = _context
                    .VwViviendasParaAlquilar
                    .OrderByDescending(r => r.FechaPublicacion)
                    .Where(r => r.IdUsuario == IdUsuario)
                    .Skip((Page - 1) * Size)
                    .Take(Size).ToList();
                foreach (VwViviendasParaAlquilar home in list)
                {
                    home.Fotos = _context.FotosXVivienda.Where(r => r.Id == home.Id).ToList();
                }
                return Ok(list);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
