﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using rentario_api.Models;

namespace rentario_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FavoritasController : ControllerBase
    {
        private readonly rentarioContext _context;

        public FavoritasController(rentarioContext context)
        {
            _context = context;
        }

        // GET: api/Favoritas
        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<Favoritas>>> GetFavoritas()
        {
            return await _context.Favoritas.ToListAsync();
        }

        // GET: api/Favoritas/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Favoritas>> GetFavoritas(int id)
        {
            var favoritas = await _context.Favoritas.FindAsync(id);

            if (favoritas == null)
            {
                return NotFound();
            }

            return favoritas;
        }

        // GET: api/GetMyFavoritas/5
        [HttpGet("GetMyFavoritas/{idUsuario}")]
        [Authorize]
        public IActionResult GetMyFavoritas(int idUsuario, [FromQuery]int Page, int Size)
        {
            var query = from a in _context.VwViviendasParaAlquilar
                        join b in _context.Favoritas
                        on a.Id equals b.IdVivienda
                        where b.IdUsuario == idUsuario
                        select a;
            List<VwViviendasParaAlquilar> casas = query.Skip((Page -1) * Size).Take(Size).ToList();
            if (casas == null)
            {
                return NotFound();
            }
            foreach (VwViviendasParaAlquilar home in casas)
            {
                home.Favorita = true;
                home.Fotos = _context.FotosXVivienda.Where(r => r.Id == home.Id).ToList();
            }
            return Ok(casas);
        }

        // PUT: api/Favoritas/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        [Authorize(Roles = "3")]
        public async Task<IActionResult> PutFavoritas(int id, Favoritas favoritas)
        {
            if (id != favoritas.Id)
            {
                return BadRequest();
            }

            _context.Entry(favoritas).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FavoritasExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Favoritas
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        [Authorize(Roles = "2")]
        public async Task<ActionResult<Favoritas>> PostFavoritas(Favoritas favoritas)
        {
            _context.Favoritas.Add(favoritas);
            await _context.SaveChangesAsync();

            return Ok();
        }

        // DELETE: api/Favoritas/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "3")]
        public async Task<ActionResult<Favoritas>> DeleteFavoritas(int id)
        {
            var favoritas = await _context.Favoritas.FindAsync(id);
            if (favoritas == null)
            {
                return NotFound();
            }

            _context.Favoritas.Remove(favoritas);
            await _context.SaveChangesAsync();

            return favoritas;
        }

        [HttpDelete("DeleteFavorita")]
        [Authorize(Roles = "2")]
        public async Task<ActionResult<Favoritas>> DeleteFavorita([FromQuery] int IdVivienda, int IdUsuario)
        {
            var favorita = await _context.Favoritas.Where(r => r.IdUsuario == IdUsuario && r.IdVivienda == IdVivienda).FirstOrDefaultAsync();
            if (favorita == null)
            {
                return NotFound();
            }

            _context.Favoritas.Remove(favorita);
            await _context.SaveChangesAsync();

            return favorita;
        }

        private bool FavoritasExists(int id)
        {
            return _context.Favoritas.Any(e => e.Id == id);
        }
    }
}
