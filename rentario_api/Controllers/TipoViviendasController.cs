﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using rentario_api.Models;

namespace rentario_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TipoViviendasController : ControllerBase
    {
        private readonly rentarioContext _context;

        public TipoViviendasController(rentarioContext context)
        {
            _context = context;
        }

        // GET: api/TipoViviendas
        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<TipoVivienda>>> GetTipoVivienda()
        {
            return await _context.TipoVivienda.ToListAsync();
        }

        // GET: api/TipoViviendas/5
        [HttpGet("{id}")]
        [Authorize(Roles = "3")]
        public async Task<ActionResult<TipoVivienda>> GetTipoVivienda(int id)
        {
            var tipoVivienda = await _context.TipoVivienda.FindAsync(id);

            if (tipoVivienda == null)
            {
                return NotFound();
            }

            return tipoVivienda;
        }

        // PUT: api/TipoViviendas/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        [Authorize(Roles = "3")]
        public async Task<IActionResult> PutTipoVivienda(int id, TipoVivienda tipoVivienda)
        {
            if (id != tipoVivienda.Id)
            {
                return BadRequest();
            }

            _context.Entry(tipoVivienda).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TipoViviendaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/TipoViviendas
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        [Authorize(Roles = "3")]
        public async Task<ActionResult<TipoVivienda>> PostTipoVivienda(TipoVivienda tipoVivienda)
        {
            _context.TipoVivienda.Add(tipoVivienda);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTipoVivienda", new { id = tipoVivienda.Id }, tipoVivienda);
        }

        // DELETE: api/TipoViviendas/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "3")]
        public async Task<ActionResult<TipoVivienda>> DeleteTipoVivienda(int id)
        {
            var tipoVivienda = await _context.TipoVivienda.FindAsync(id);
            if (tipoVivienda == null)
            {
                return NotFound();
            }

            _context.TipoVivienda.Remove(tipoVivienda);
            await _context.SaveChangesAsync();

            return tipoVivienda;
        }

        private bool TipoViviendaExists(int id)
        {
            return _context.TipoVivienda.Any(e => e.Id == id);
        }
    }
}
