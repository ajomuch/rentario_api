﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using rentario_api.Models;

namespace rentario_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DivisionPaisController : ControllerBase
    {
        private readonly rentarioContext _context;

        public DivisionPaisController(rentarioContext context)
        {
            _context = context;
        }

        // GET: api/DivisionPais
        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<DivisionPais>>> GetDivisionPais()
        {
            return await _context.DivisionPais.ToListAsync();
        }

        // GET: api/DivisionPais/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DivisionPais>> GetDivisionPais(int id)
        {
            var divisionPais = await _context.DivisionPais.FindAsync(id);

            if (divisionPais == null)
            {
                return NotFound();
            }

            return divisionPais;
        }

        // PUT: api/DivisionPais/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        [Authorize(Roles = "3")]
        public async Task<IActionResult> PutDivisionPais(int id, DivisionPais divisionPais)
        {
            if (id != divisionPais.Id)
            {
                return BadRequest();
            }

            _context.Entry(divisionPais).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DivisionPaisExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/DivisionPais
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        [Authorize(Roles = "3")]
        public async Task<ActionResult<DivisionPais>> PostDivisionPais(DivisionPais divisionPais)
        {
            _context.DivisionPais.Add(divisionPais);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDivisionPais", new { id = divisionPais.Id }, divisionPais);
        }

        // DELETE: api/DivisionPais/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "3")]
        public async Task<ActionResult<DivisionPais>> DeleteDivisionPais(int id)
        {
            var divisionPais = await _context.DivisionPais.FindAsync(id);
            if (divisionPais == null)
            {
                return NotFound();
            }

            _context.DivisionPais.Remove(divisionPais);
            await _context.SaveChangesAsync();

            return divisionPais;
        }

        private bool DivisionPaisExists(int id)
        {
            return _context.DivisionPais.Any(e => e.Id == id);
        }
    }
}
