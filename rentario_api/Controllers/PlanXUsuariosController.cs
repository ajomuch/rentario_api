﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using rentario_api.Models;

namespace rentario_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlanXUsuariosController : ControllerBase
    {
        private readonly rentarioContext _context;

        public PlanXUsuariosController(rentarioContext context)
        {
            _context = context;
        }

        // GET: api/PlanXUsuarios
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IEnumerable<PlanXUsuario>>> GetPlanXUsuario()
        {
            return await _context.PlanXUsuario.ToListAsync();
        }

        // GET: api/PlanXUsuarios/5
        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<PlanXUsuario>> GetPlanXUsuario(int id)
        {
            var planXUsuario = await _context.PlanXUsuario.FindAsync(id);

            if (planXUsuario == null)
            {
                return NotFound();
            }

            return planXUsuario;
        }

        // PUT: api/PlanXUsuarios/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        [Authorize(Roles = "3")]
        public async Task<IActionResult> PutPlanXUsuario(int id, PlanXUsuario planXUsuario)
        {
            if (id != planXUsuario.Id)
            {
                return BadRequest();
            }

            _context.Entry(planXUsuario).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PlanXUsuarioExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/PlanXUsuarios
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        [Authorize(Roles = "3")]
        public async Task<ActionResult<PlanXUsuario>> PostPlanXUsuario(PlanXUsuario planXUsuario)
        {
            _context.PlanXUsuario.Add(planXUsuario);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPlanXUsuario", new { id = planXUsuario.Id }, planXUsuario);
        }

        // DELETE: api/PlanXUsuarios/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "3")]
        public async Task<ActionResult<PlanXUsuario>> DeletePlanXUsuario(int id)
        {
            var planXUsuario = await _context.PlanXUsuario.FindAsync(id);
            if (planXUsuario == null)
            {
                return NotFound();
            }

            _context.PlanXUsuario.Remove(planXUsuario);
            await _context.SaveChangesAsync();

            return planXUsuario;
        }

        private bool PlanXUsuarioExists(int id)
        {
            return _context.PlanXUsuario.Any(e => e.Id == id);
        }
    }
}
