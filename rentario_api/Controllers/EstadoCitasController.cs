﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using rentario_api.Models;

namespace rentario_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EstadoCitasController : ControllerBase
    {
        private readonly rentarioContext _context;

        public EstadoCitasController(rentarioContext context)
        {
            _context = context;
        }

        // GET: api/EstadoCitas
        [HttpGet]
        public async Task<ActionResult<IEnumerable<EstadoCita>>> GetEstadoCita()
        {
            return await _context.EstadoCita.ToListAsync();
        }

        // GET: api/EstadoCitas/5
        [HttpGet("{id}")]
        [Authorize(Roles = "3")]
        public async Task<ActionResult<EstadoCita>> GetEstadoCita(int id)
        {
            var estadoCita = await _context.EstadoCita.FindAsync(id);

            if (estadoCita == null)
            {
                return NotFound();
            }

            return estadoCita;
        }

        // PUT: api/EstadoCitas/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        [Authorize(Roles = "3")]
        public async Task<IActionResult> PutEstadoCita(int id, EstadoCita estadoCita)
        {
            if (id != estadoCita.Id)
            {
                return BadRequest();
            }

            _context.Entry(estadoCita).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EstadoCitaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/EstadoCitas
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        [Authorize(Roles = "3")]
        public async Task<ActionResult<EstadoCita>> PostEstadoCita(EstadoCita estadoCita)
        {
            _context.EstadoCita.Add(estadoCita);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetEstadoCita", new { id = estadoCita.Id }, estadoCita);
        }

        // DELETE: api/EstadoCitas/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "3")]
        public async Task<ActionResult<EstadoCita>> DeleteEstadoCita(int id)
        {
            var estadoCita = await _context.EstadoCita.FindAsync(id);
            if (estadoCita == null)
            {
                return NotFound();
            }

            _context.EstadoCita.Remove(estadoCita);
            await _context.SaveChangesAsync();

            return estadoCita;
        }

        private bool EstadoCitaExists(int id)
        {
            return _context.EstadoCita.Any(e => e.Id == id);
        }
    }
}
