﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using rentario_api.Models;

namespace rentario_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ViviendasParaAlquilarsController : ControllerBase
    {
        private readonly rentarioContext _context;

        public ViviendasParaAlquilarsController(rentarioContext context)
        {
            _context = context;
        }

        // GET: api/ViviendasParaAlquilars
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IEnumerable<ViviendasParaAlquilar>>> GetViviendasParaAlquilar()
        {
            return await _context.ViviendasParaAlquilar.ToListAsync();
        }

        // GET: api/ViviendasParaAlquilars/5
        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<ViviendasParaAlquilar>> GetViviendasParaAlquilar(int id)
        {
            var viviendasParaAlquilar = await _context.ViviendasParaAlquilar.Include(r => r.FotosXVivienda).FirstOrDefaultAsync(r => r.Id == id);

            if (viviendasParaAlquilar == null)
            {
                return NotFound();
            }

            return viviendasParaAlquilar;
        }

        // PUT: api/ViviendasParaAlquilars/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        [Authorize]
        public async Task<IActionResult> PutViviendasParaAlquilar(int id, ViviendasParaAlquilar viviendasParaAlquilar)
        {
            if (id != viviendasParaAlquilar.Id)
            {
                return BadRequest();
            }

            _context.Entry(viviendasParaAlquilar).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ViviendasParaAlquilarExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ViviendasParaAlquilars
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<ViviendasParaAlquilar>> PostViviendasParaAlquilar(ViviendasParaAlquilar viviendasParaAlquilar)
        {
            try
            {
                _context.ViviendasParaAlquilar.Add(viviendasParaAlquilar);
                await _context.SaveChangesAsync();
                return Ok(viviendasParaAlquilar);
            }
            catch(Exception ex)
            {
                return StatusCode(500, ex.Message);
            }            
        }

        // DELETE: api/ViviendasParaAlquilars/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<ActionResult<ViviendasParaAlquilar>> DeleteViviendasParaAlquilar(int id)
        {
            var viviendasParaAlquilar = await _context.ViviendasParaAlquilar.FindAsync(id);
            if (viviendasParaAlquilar == null)
            {
                return NotFound();
            }

            _context.ViviendasParaAlquilar.Remove(viviendasParaAlquilar);
            await _context.SaveChangesAsync();

            return viviendasParaAlquilar;
        }

        private bool ViviendasParaAlquilarExists(int id)
        {
            return _context.ViviendasParaAlquilar.Any(e => e.Id == id);
        }
    }
}
