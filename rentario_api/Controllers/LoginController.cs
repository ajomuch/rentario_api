﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using rentario_api.Email;
using rentario_api.Models;
using rentario_api.Utils;

namespace rentario_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly rentarioContext _context;
        private readonly IConfiguration _config;
        private readonly IMailService _mailService;
        public LoginController(rentarioContext context, IConfiguration config, IMailService mailService)
        {
            _context = context;
            _config = config;
            _mailService = mailService;
        }

        // POST: api/Login
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        [AllowAnonymous]
        public IActionResult PostUsuarios(Usuarios usuario)
        {
            Usuarios _usuarioChequeado = checkUser(usuario);
            if(_usuarioChequeado != null)
            {
                _usuarioChequeado.Token = generateJwt(_usuarioChequeado, JwtType.longTime);
                return Ok(_usuarioChequeado);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult GetCode([FromQuery] string Email)
        {
            try
            {
                Usuarios usuario = _context.Usuarios.Where(r => r.Correo == Email).FirstOrDefault();
                if (usuario != null)
                {
                    String code = generateRandomString(5);
                    usuario.CodigoRecuperacion = code;
                    string jwt = generateJwt(usuario, JwtType.shortTime);
                    _mailService.SendEmailAsync(usuario.Correo, code);
                    _context.SaveChanges();
                    return Ok(new { code, jwt });
                }
                else
                {
                    return NotFound();
                }
            }
            catch(Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost("ResetPassword")]
        [Authorize(Roles = "recovery")]
        public IActionResult ResetPassword([FromBody] Usuarios usuario)
        {
            try
            {
                Usuarios _usuario = _context.Usuarios.Where(r => r.Correo == usuario.Correo && r.CodigoRecuperacion == usuario.CodigoRecuperacion).FirstOrDefault();
                if (_usuario != null)
                {
                    _usuario.CodigoRecuperacion = "";
                    String password = Encrypt.TextToHash(usuario.Contrasenia);
                    _usuario.Contrasenia = password;
                    _context.SaveChanges();
                    return Ok();
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        private Usuarios checkUser(Usuarios user)
        {
            String contrasenia = Encrypt.TextToHash(user.Contrasenia);
            try
            {
                Usuarios _newUser = _context.Usuarios.Where(r => r.Correo == user.Correo && r.Contrasenia == contrasenia).Select(r => new Usuarios
                {
                    Nombre = r.Nombre,
                    UrlPerfil = r.UrlPerfil,
                    PrimeraVez = r.PrimeraVez,
                    Id = r.Id,
                    Correo = r.Correo,
                    TipoUsuario = r.TipoUsuario,
                    Genero = r.Genero,
                    Telefono = r.Telefono,
                    FechaNacimiento = r.FechaNacimiento,
                    ContactarPorWhatsapp = r.ContactarPorWhatsapp
                }).FirstOrDefault();
                return _newUser;
            }catch(Exception ex)
            {
                throw ex;
            }
        }

        private string generateJwt(Usuarios usuario, JwtType type)
        {
            SymmetricSecurityKey symetricKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["JWT:Secret"]));
            SigningCredentials signingCredentials = new SigningCredentials(symetricKey, SecurityAlgorithms.HmacSha256);
            JwtHeader header = new JwtHeader(signingCredentials);
            List<Claim> claims = new List<Claim>();
            claims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
            claims.Add(new Claim(JwtRegisteredClaimNames.NameId, usuario.Id.ToString()));
            claims.Add(new Claim("name", usuario.Nombre));
            claims.Add(new Claim("correo", usuario.Correo));
            JwtPayload payload;
            if(type == JwtType.longTime)
            {
                claims.Add(new Claim(ClaimTypes.Role, usuario.TipoUsuario.ToString()));
                payload = new JwtPayload(_config["JWT:ValidIssuer"], _config["JWT:ValidAudience"], claims, DateTime.Now, DateTime.UtcNow.AddMonths(12));
            }
            else
            {
                claims.Add(new Claim(ClaimTypes.Role, "recovery"));
                payload = new JwtPayload(_config["JWT:ValidIssuer"], _config["JWT:ValidAudience"], claims, DateTime.Now, DateTime.UtcNow.AddMinutes(10));
            }
            JwtSecurityToken token = new JwtSecurityToken(header, payload);
            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private string generateRandomString(int length)
        {
            Random random = new Random();
            string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }

    enum JwtType
    {
        shortTime,
        longTime
    }
}
