﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using rentario_api.Models;
using rentario_api.Utils;

namespace rentario_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuariosController : ControllerBase
    {
        private readonly rentarioContext _context;

        public UsuariosController(rentarioContext context)
        {
            _context = context;
        }

        // GET: api/Usuarios
        [HttpGet]
        [Authorize(Roles = "3")]
        public async Task<ActionResult<IEnumerable<Usuarios>>> GetUsuarios()
        {
            return await _context.Usuarios.ToListAsync();
        }

        // GET: api/Usuarios/5
        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<Usuarios>> GetUsuarios(int id)
        {
            var usuarios = await _context.Usuarios.FindAsync(id);

            if (usuarios == null)
            {
                return NotFound();
            }

            return usuarios;
        }

        // PUT: api/Usuarios/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        [Authorize]
        public async Task<IActionResult> PutUsuarios(int id, Usuarios usuarios)
        {
            if (id != usuarios.Id)
            {
                return BadRequest();
            }

            _context.Entry(usuarios).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UsuariosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Usuarios
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult<Usuarios>> PostUsuarios(Usuarios usuario)
        {
            if(_context.Usuarios.Any(r => r.Correo == usuario.Correo))
            {
                return BadRequest("Usuario ya registrado");
            }
            usuario.PrimeraVez = true;
            usuario.FechaCreacion = DateTime.Now;
            usuario.Contrasenia = Encrypt.TextToHash(usuario.Contrasenia);
            _context.Usuarios.Add(usuario);
            await _context.SaveChangesAsync();
            PlanXUsuario plan = new PlanXUsuario();
            plan.IdPlan = Constants.Constants.FreePlan;
            plan.IdUsuario = usuario.Id;
            plan.FechaContratacion = usuario.FechaCreacion;
            plan.Actual = true;
            _context.PlanXUsuario.Add(plan);
            _context.SaveChanges();
            return Ok(usuario);
        }

        // DELETE: api/Usuarios/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<ActionResult<Usuarios>> DeleteUsuarios(int id)
        {
            var usuarios = await _context.Usuarios.FindAsync(id);
            if (usuarios == null)
            {
                return NotFound();
            }

            _context.Usuarios.Remove(usuarios);
            await _context.SaveChangesAsync();

            return usuarios;
        }

        private bool UsuariosExists(int id)
        {
            return _context.Usuarios.Any(e => e.Id == id);
        }

        [HttpPatch("{id:int}")]
        [Authorize]
        public IActionResult Patch(int id, [FromBody] JsonPatchDocument<Usuarios> patchUsuario)
        {
            try
            {
                Usuarios currentUser = _context.Usuarios.Where(r => r.Id == id).FirstOrDefault();
                if (currentUser == null)
                    return NotFound();
                patchUsuario.ApplyTo(currentUser, ModelState);
                _context.SaveChanges();
                return Ok(currentUser);
            }
            catch(Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("GetMyCalification")]
        [Authorize]
        public IActionResult GetMyCalification([FromQuery]int IdUsuario)
        {
            try
            {
                List<int> puntuaciones = _context.PuntuacionPorVivienda.Where(r => r.IdUsuario == IdUsuario).Select(r => r.Puntuacion).ToList();
                if (puntuaciones.Count == 0)
                    return Ok(0);
                return Ok(puntuaciones.Average());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost("UpdatePassword")]
        [Authorize]
        public IActionResult UpdatePassword([FromBody]Usuarios usuario)
        {
            try
            {
                string contrasenia = Encrypt.TextToHash(usuario.Contrasenia);
                Usuarios usuarioActual = _context.Usuarios.Where(r => r.Id == usuario.Id && r.Contrasenia == contrasenia).FirstOrDefault();
                if (usuarioActual == null)
                    return NotFound();
                usuarioActual.Contrasenia = Encrypt.TextToHash(usuario.NuevaContrasenia);
                _context.Entry(usuarioActual).State = EntityState.Modified;
                _context.SaveChanges();
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
