﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using rentario_api.Models;

namespace rentario_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TipoUsuariosController : ControllerBase
    {
        private readonly rentarioContext _context;

        public TipoUsuariosController(rentarioContext context)
        {
            _context = context;
        }

        // GET: api/TipoUsuarios
        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<TipoUsuarios>>> GetTipoUsuarios()
        {
            return await _context.TipoUsuarios.Where(r => r.Id  != Constants.Constants.AdminCode).ToListAsync();
        }

        // GET: api/TipoUsuarios/5
        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<TipoUsuarios>> GetTipoUsuarios(int id)
        {
            var tipoUsuarios = await _context.TipoUsuarios.FindAsync(id);

            if (tipoUsuarios == null)
            {
                return NotFound();
            }

            return tipoUsuarios;
        }

        // PUT: api/TipoUsuarios/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        [Authorize(Roles = "3")]
        public async Task<IActionResult> PutTipoUsuarios(int id, TipoUsuarios tipoUsuarios)
        {
            if (id != tipoUsuarios.Id)
            {
                return BadRequest();
            }

            _context.Entry(tipoUsuarios).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TipoUsuariosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/TipoUsuarios
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        [Authorize(Roles = "3")]
        public async Task<ActionResult<TipoUsuarios>> PostTipoUsuarios(TipoUsuarios tipoUsuarios)
        {
            _context.TipoUsuarios.Add(tipoUsuarios);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (TipoUsuariosExists(tipoUsuarios.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetTipoUsuarios", new { id = tipoUsuarios.Id }, tipoUsuarios);
        }

        // DELETE: api/TipoUsuarios/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "3")]
        public async Task<ActionResult<TipoUsuarios>> DeleteTipoUsuarios(int id)
        {
            var tipoUsuarios = await _context.TipoUsuarios.FindAsync(id);
            if (tipoUsuarios == null)
            {
                return NotFound();
            }

            _context.TipoUsuarios.Remove(tipoUsuarios);
            await _context.SaveChangesAsync();

            return tipoUsuarios;
        }

        private bool TipoUsuariosExists(int id)
        {
            return _context.TipoUsuarios.Any(e => e.Id == id);
        }
    }
}
