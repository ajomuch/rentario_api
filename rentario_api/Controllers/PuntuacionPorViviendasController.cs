﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using rentario_api.Models;

namespace rentario_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PuntuacionPorViviendasController : ControllerBase
    {
        private readonly rentarioContext _context;

        public PuntuacionPorViviendasController(rentarioContext context)
        {
            _context = context;
        }

        // GET: api/PuntuacionPorViviendas
        [HttpGet]
        //[Authorize]
        public async Task<ActionResult<IEnumerable<VwPuntuacionPorVivienda>>> GetPuntuacionPorVivienda([FromQuery]int IdVivienda)
        {
            return await _context.VwPuntuacionPorVivienda.Where(r => r.IdVivienda == IdVivienda).ToListAsync();
        }

        // GET: api/PuntuacionPorViviendas/5
        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<PuntuacionPorVivienda>> GetPuntuacionPorVivienda2(int id)
        {
            var puntuacionPorVivienda = await _context.PuntuacionPorVivienda.FindAsync(id);

            if (puntuacionPorVivienda == null)
            {
                return NotFound();
            }

            return puntuacionPorVivienda;
        }

        // PUT: api/PuntuacionPorViviendas/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        [Authorize]
        public async Task<IActionResult> PutPuntuacionPorVivienda(int id, PuntuacionPorVivienda puntuacionPorVivienda)
        {
            if (id != puntuacionPorVivienda.Id)
            {
                return BadRequest();
            }

            _context.Entry(puntuacionPorVivienda).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PuntuacionPorViviendaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/PuntuacionPorViviendas
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<PuntuacionPorVivienda>> PostPuntuacionPorVivienda(PuntuacionPorVivienda puntuacionPorVivienda)
        {
            _context.PuntuacionPorVivienda.Add(puntuacionPorVivienda);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPuntuacionPorVivienda", new { id = puntuacionPorVivienda.Id }, puntuacionPorVivienda);
        }

        // DELETE: api/PuntuacionPorViviendas/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<ActionResult<PuntuacionPorVivienda>> DeletePuntuacionPorVivienda(int id)
        {
            var puntuacionPorVivienda = await _context.PuntuacionPorVivienda.FindAsync(id);
            if (puntuacionPorVivienda == null)
            {
                return NotFound();
            }

            _context.PuntuacionPorVivienda.Remove(puntuacionPorVivienda);
            await _context.SaveChangesAsync();

            return puntuacionPorVivienda;
        }

        private bool PuntuacionPorViviendaExists(int id)
        {
            return _context.PuntuacionPorVivienda.Any(e => e.Id == id);
        }
    }
}
