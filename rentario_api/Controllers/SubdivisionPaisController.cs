﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using rentario_api.Models;

namespace rentario_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubdivisionPaisController : ControllerBase
    {
        private readonly rentarioContext _context;

        public SubdivisionPaisController(rentarioContext context)
        {
            _context = context;
        }

        // GET: api/SubdivisionPais
        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<SubdivisionPais>>> GetSubdivisionPais()
        {
            return await _context.SubdivisionPais.ToListAsync();
        }

        // GET: api/SubdivisionPais/5
        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<SubdivisionPais>> GetSubdivisionPais(int id)
        {
            var subdivisionPais = await _context.SubdivisionPais.FindAsync(id);

            if (subdivisionPais == null)
            {
                return NotFound();
            }

            return subdivisionPais;
        }

        // PUT: api/SubdivisionPais/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        [Authorize(Roles = "3")]
        public async Task<IActionResult> PutSubdivisionPais(int id, SubdivisionPais subdivisionPais)
        {
            if (id != subdivisionPais.Id)
            {
                return BadRequest();
            }

            _context.Entry(subdivisionPais).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SubdivisionPaisExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/SubdivisionPais
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        [Authorize(Roles = "3")]
        public async Task<ActionResult<SubdivisionPais>> PostSubdivisionPais(SubdivisionPais subdivisionPais)
        {
            _context.SubdivisionPais.Add(subdivisionPais);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSubdivisionPais", new { id = subdivisionPais.Id }, subdivisionPais);
        }

        // DELETE: api/SubdivisionPais/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "3")]
        public async Task<ActionResult<SubdivisionPais>> DeleteSubdivisionPais(int id)
        {
            var subdivisionPais = await _context.SubdivisionPais.FindAsync(id);
            if (subdivisionPais == null)
            {
                return NotFound();
            }

            _context.SubdivisionPais.Remove(subdivisionPais);
            await _context.SaveChangesAsync();

            return subdivisionPais;
        }

        private bool SubdivisionPaisExists(int id)
        {
            return _context.SubdivisionPais.Any(e => e.Id == id);
        }
    }
}
