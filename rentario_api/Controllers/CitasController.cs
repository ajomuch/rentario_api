﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using rentario_api.Models;
using rentario_api.Utils;

namespace rentario_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CitasController : ControllerBase
    {
        private readonly rentarioContext _context;
        private readonly IConfiguration _configuration;
        private readonly PushNotification pushNotification = new PushNotification();

        public CitasController(rentarioContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        // GET: api/Citas
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IEnumerable<Citas>>> GetCitas()
        {
            return await _context.Citas.ToListAsync();
        }

        [HttpGet("GetCitasByUser")]
        [Authorize]
        public async Task<ActionResult<IEnumerable<VwCitas>>> GetCitasByUser([FromQuery] int IdUsuario, int Page, int Size)
        {
            try
            {
                var list = await (from a in _context.VwCitas
                           join b in _context.ViviendasParaAlquilar
                           on a.IdVivienda equals b.Id
                           where b.IdUsuario == IdUsuario
                           orderby a.FechaCreacion descending
                           select a)
                            .Skip(Page * Size)
                            .Take(Size)
                            .ToListAsync();
                if(list == null)
                {
                    return NotFound();
                }
                return list;
            }
            catch(Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("GetCitasByClient")]
        [Authorize]
        public async Task<ActionResult<IEnumerable<VwCitas>>> GetCitasByClient([FromQuery] int IdCliente, int Page, int Size)
        {
            try
            {
                var list = await _context.VwCitas.Where(r => r.IdCliente == IdCliente)
                            .Skip(Page * Size)
                            .Take(Size)
                            .OrderByDescending(r => r.FechaCreacion)
                            .ToListAsync();
                if (list == null)
                {
                    return NotFound();
                }
                return list;
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPatch("{id:int}")]
        [Authorize]
        public IActionResult Patch(int id, [FromBody] JsonPatchDocument<Citas> patchCita)
        {
            try
            {
                Citas currentCita = _context.Citas.Include(r => r.IdViviendaNavigation).Where(r => r.Id == id).FirstOrDefault();
                if (currentCita == null)
                    return NotFound();
                patchCita.ApplyTo(currentCita, ModelState);
                _context.SaveChanges();
                string token;
                if (currentCita.Arrendatario)
                {
                    int idUsuario = currentCita.IdViviendaNavigation.IdUsuario;
                    token = _context.Usuarios.Where(r => r.Id == idUsuario).Select(r => r.FcmToken).FirstOrDefault();
                }
                else
                {
                    int idUsuario = currentCita.IdCliente;
                    token = _context.Usuarios.Where(r => r.Id == idUsuario).Select(r => r.FcmToken).FirstOrDefault();
                }
                sendNotification(token);
                return Ok(currentCita);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // GET: api/Citas/5
        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<Citas>> GetCitas(int id)
        {
            var citas = await _context.Citas.FindAsync(id);

            if (citas == null)
            {
                return NotFound();
            }

            return citas;
        }

        // PUT: api/Citas/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        [Authorize(Roles = "2")]
        public async Task<IActionResult> PutCitas(int id, Citas citas)
        {
            if (id != citas.Id)
            {
                return BadRequest();
            }

            _context.Entry(citas).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CitasExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Citas
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        [Authorize(Roles = "2")]
        public async Task<ActionResult<Citas>> PostCitas(Citas citas)
        {
            _context.Citas.Add(citas);
            var query = from a in _context.Usuarios
                        join b in _context.ViviendasParaAlquilar
                        on a.Id equals b.IdUsuario
                        where b.Id == citas.IdVivienda
                        select a;
            Usuarios usuario = query.FirstOrDefault();
            if(usuario != null)
            {
                if(!string.IsNullOrEmpty(usuario.FcmToken))
                {
                    string serverKey = _configuration.GetValue<string>("ServerKey");
                    string senderId = _configuration.GetValue<string>("SenderId");
                    Random rnd = new Random();
                    var notification = new
                    {
                        notification = new {
                            body = "Has recibido una nueva cita para ver tu casa",
                            title = "Nueva cita"
                        },
                        data = new {
                            click_action = "FLUTTER_NOTIFICATION_CLICK",
                            id = rnd.Next(0, 10000000),
                            status = "done"
                        },
                        priority = "High",
                        to = usuario.FcmToken
                    };
                    await pushNotification.sendNotification(serverKey, senderId, usuario.FcmToken, notification);
                }
            }
            await _context.SaveChangesAsync();

            return Ok();
        }

        // DELETE: api/Citas/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "2,3")]
        public async Task<ActionResult<Citas>> DeleteCitas(int id)
        {
            var citas = await _context.Citas.FindAsync(id);
            if (citas == null)
            {
                return NotFound();
            }

            _context.Citas.Remove(citas);
            await _context.SaveChangesAsync();

            return citas;
        }



        private bool CitasExists(int id)
        {
            return _context.Citas.Any(e => e.Id == id);
        }

        private void sendNotification(string fcmToken)
        {
            try
            {
                if (string.IsNullOrEmpty(fcmToken))
                    throw new Exception("Null token");
                string serverKey = _configuration.GetValue<string>("ServerKey");
                string senderId = _configuration.GetValue<string>("SenderId");
                Random rnd = new Random();
                var notification = new
                {
                    notification = new
                    {
                        body = "Una de tus citas fue actualizada, ve y chequéala.",
                        title = "Cita actualizada"
                    },
                    data = new
                    {
                        click_action = "FLUTTER_NOTIFICATION_CLICK",
                        id = rnd.Next(0, 10000000),
                        status = "done"
                    },
                    priority = "High",
                    to = fcmToken
                };
                pushNotification.sendNotification(serverKey, senderId, fcmToken, notification);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
