﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using rentario_api.Models;

namespace rentario_api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class MiscellaneousController : ControllerBase
    {
        private rentarioContext _context;

        public MiscellaneousController(rentarioContext context)
        {
            _context = context;
        }

        [HttpGet]
        [ActionName("GetDataToAddHome")]
        public IActionResult GetDataToAddHome()
        {
            try
            {
                Dictionary<string, object> response = new Dictionary<string, object>();
                response.Add("tipoVivienda", _context.TipoVivienda.ToList());
                response.Add("pais", _context.Paises.ToList());
                return new JsonResult(response);
            }
            catch(Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet]
        [ActionName("GetDivisionByCountry")]
        public IActionResult GetDivisionByCountry([FromQuery] int Id)
        {
            try
            {
                List<DivisionPais> list = _context.DivisionPais.Where(r => r.IdPais == Id).ToList();
                return new JsonResult(list);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet]
        [ActionName("GetSubDivisionByDivision")]
        public IActionResult GetSubDivisionByDivision([FromQuery] int Id)
        {
            try
            {
                List<SubdivisionPais> list = _context.SubdivisionPais.Where(r => r.IdDivision == Id).ToList();
                return new JsonResult(list);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet]
        [ActionName("GetCompletePlaceTree")]
        public IActionResult GetCompletePlaceTree([FromQuery] int Id)
        {
            try
            {
                SubdivisionPais _subdivision = _context.SubdivisionPais.Where(r => r.Id == Id).Include(r => r.IdDivisionNavigation).FirstOrDefault();
                List<SubdivisionPais> subdivision = _context.SubdivisionPais.Where(r => r.IdDivision == _subdivision.IdDivision).ToList();
                List<DivisionPais> division = _context.DivisionPais.Where(r => r.IdPais == _subdivision.IdDivisionNavigation.IdPais).ToList();
                List<Paises> paises = _context.Paises.ToList();
                return new JsonResult(new { paises, idDivision = _subdivision.IdDivisionNavigation.Id, idPais = _subdivision.IdDivisionNavigation.IdPais });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
