﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace rentario_api.Models
{
    public partial class Usuarios
    {
        [NotMapped]
        public string Token { get; set; }
        [NotMapped]
        public string NuevaContrasenia { get; set; }
    }
}
