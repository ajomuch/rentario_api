﻿using System;
using System.Collections.Generic;

namespace rentario_api.Models
{
    public partial class SubdivisionPais
    {
        public SubdivisionPais()
        {
            ViviendasParaAlquilar = new HashSet<ViviendasParaAlquilar>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public int IdDivision { get; set; }

        public virtual DivisionPais IdDivisionNavigation { get; set; }
        public virtual ICollection<ViviendasParaAlquilar> ViviendasParaAlquilar { get; set; }
    }
}
