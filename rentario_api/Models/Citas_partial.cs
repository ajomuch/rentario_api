﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace rentario_api.Models
{
    public partial class Citas
    {
        [NotMapped]
        public bool Arrendatario { get; set; }
    }
}
