﻿using System;
using System.Collections.Generic;

namespace rentario_api.Models
{
    public partial class TipoVivienda
    {
        public TipoVivienda()
        {
            ViviendasParaAlquilar = new HashSet<ViviendasParaAlquilar>();
        }

        public int Id { get; set; }
        public string TipoVivienda1 { get; set; }

        public virtual ICollection<ViviendasParaAlquilar> ViviendasParaAlquilar { get; set; }
    }
}
