﻿using System;
using System.Collections.Generic;

namespace rentario_api.Models
{
    public partial class TipoUsuarios
    {
        public TipoUsuarios()
        {
            Usuarios = new HashSet<Usuarios>();
        }

        public int Id { get; set; }
        public string TipoUsuario { get; set; }
        public bool Estado { get; set; }

        public virtual ICollection<Usuarios> Usuarios { get; set; }
    }
}
