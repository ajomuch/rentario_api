﻿using System;
using System.Collections.Generic;

namespace rentario_api.Models
{
    public partial class Favoritas
    {
        public int Id { get; set; }
        public int IdUsuario { get; set; }
        public int IdVivienda { get; set; }
        public DateTime Fecha { get; set; }

        public virtual Usuarios IdUsuarioNavigation { get; set; }
        public virtual ViviendasParaAlquilar IdViviendaNavigation { get; set; }
    }
}
