﻿using System;
using System.Collections.Generic;

namespace rentario_api.Models
{
    public partial class PlanXUsuario
    {
        public int Id { get; set; }
        public int IdUsuario { get; set; }
        public int IdPlan { get; set; }
        public DateTime FechaContratacion { get; set; }
        public bool Actual { get; set; }

        public virtual Planes IdPlanNavigation { get; set; }
        public virtual Usuarios IdUsuarioNavigation { get; set; }
    }
}
