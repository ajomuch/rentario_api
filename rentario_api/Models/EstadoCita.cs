﻿using System;
using System.Collections.Generic;

namespace rentario_api.Models
{
    public partial class EstadoCita
    {
        public EstadoCita()
        {
            Citas = new HashSet<Citas>();
        }

        public int Id { get; set; }
        public string Estado { get; set; }
        public string Accion { get; set; }

        public virtual ICollection<Citas> Citas { get; set; }
    }
}
