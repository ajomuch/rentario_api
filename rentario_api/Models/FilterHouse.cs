﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rentario_api.Models
{
    public class FilterHouse
    {
        public int? IdTipoVivienda { get; set; } 
        public string Colonia { get; set; }
        public int? Dormitorios { get; set; }
        public double? PrecioInicial { get; set; }
        public double? PrecioFinal { get; set; }
        public double? Puntuacion { get; set; }
        public int? IdPais { get; set; }
        public int? IdDivision { get; set; }
        public int? IdSubdivision { get; set; }

    }
}
