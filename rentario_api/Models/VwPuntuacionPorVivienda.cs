﻿using System;
using System.Collections.Generic;

namespace rentario_api.Models
{
    public partial class VwPuntuacionPorVivienda
    {
        public int? Id { get; set; }
        public int? IdUsuario { get; set; }
        public int? IdVivienda { get; set; }
        public string Comentario { get; set; }
        public int? Puntuacion { get; set; }
        public DateTime? Fecha { get; set; }
        public string Nombreusuario { get; set; }
        public string UrlPerfil { get; set; }
    }
}
