﻿using System;
using System.Collections.Generic;

namespace rentario_api.Models
{
    public partial class Planes
    {
        public Planes()
        {
            PlanXUsuario = new HashSet<PlanXUsuario>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public float Precio { get; set; }
        public int DuracionDias { get; set; }

        public virtual ICollection<PlanXUsuario> PlanXUsuario { get; set; }
    }
}
