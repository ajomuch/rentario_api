﻿using System;
using System.Collections.Generic;

namespace rentario_api.Models
{
    public partial class ViviendasParaAlquilar
    {
        public ViviendasParaAlquilar()
        {
            Citas = new HashSet<Citas>();
            Favoritas = new HashSet<Favoritas>();
            FotosXVivienda = new HashSet<FotosXVivienda>();
            PuntuacionPorVivienda = new HashSet<PuntuacionPorVivienda>();
        }

        public int Id { get; set; }
        public string Colonia { get; set; }
        public int? DireccionExacta { get; set; }
        public float Latitud { get; set; }
        public float Longitud { get; set; }
        public string Descripcion { get; set; }
        public int? Dormitorios { get; set; }
        public bool Estado { get; set; }
        public int IdUsuario { get; set; }
        public int IdSubdivision { get; set; }
        public float Precio { get; set; }
        public int IdTipoVivienda { get; set; }
        public DateTime FechaPublicacion { get; set; }

        public virtual SubdivisionPais IdSubdivisionNavigation { get; set; }
        public virtual TipoVivienda IdTipoViviendaNavigation { get; set; }
        public virtual Usuarios IdUsuarioNavigation { get; set; }
        public virtual ICollection<Citas> Citas { get; set; }
        public virtual ICollection<Favoritas> Favoritas { get; set; }
        public virtual ICollection<FotosXVivienda> FotosXVivienda { get; set; }
        public virtual ICollection<PuntuacionPorVivienda> PuntuacionPorVivienda { get; set; }
    }
}
