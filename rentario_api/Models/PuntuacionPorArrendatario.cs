﻿using System;
using System.Collections.Generic;

namespace rentario_api.Models
{
    public partial class PuntuacionPorArrendatario
    {
        public int Id { get; set; }
        public int IdArrendatario { get; set; }
        public int IdCliente { get; set; }
        public string Comentario { get; set; }
        public int Puntuacion { get; set; }
        public DateTime Fecha { get; set; }

        public virtual Usuarios IdArrendatarioNavigation { get; set; }
        public virtual Usuarios IdClienteNavigation { get; set; }
    }
}
