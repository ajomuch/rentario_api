﻿using System;
using System.Collections.Generic;

namespace rentario_api.Models
{
    public partial class FotosXVivienda
    {
        public int Id { get; set; }
        public int IdVivienda { get; set; }
        public string Url { get; set; }

        public virtual ViviendasParaAlquilar IdViviendaNavigation { get; set; }
    }
}
