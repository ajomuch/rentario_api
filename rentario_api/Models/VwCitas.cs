﻿using System;
using System.Collections.Generic;

namespace rentario_api.Models
{
    public partial class VwCitas
    {
        public int? Id { get; set; }
        public int? IdVivienda { get; set; }
        public int? IdCliente { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public DateTime? FechaCita { get; set; }
        public int? Estado { get; set; }
        public string Comentario { get; set; }
        public string ComentarioArrendatario { get; set; }
        public string EstadoCita { get; set; }
        public string Cliente { get; set; }
        public string Telefono { get; set; }
        public string UrlPerfil { get; set; }
        public bool? ContactarPorWhatsapp { get; set; }
    }
}
