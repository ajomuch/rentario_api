﻿using System;
using System.Collections.Generic;

namespace rentario_api.Models
{
    public partial class DivisionPais
    {
        public DivisionPais()
        {
            SubdivisionPais = new HashSet<SubdivisionPais>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public int IdPais { get; set; }

        public virtual Paises IdPaisNavigation { get; set; }
        public virtual ICollection<SubdivisionPais> SubdivisionPais { get; set; }
    }
}
