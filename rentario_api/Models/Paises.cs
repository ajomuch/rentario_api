﻿using System;
using System.Collections.Generic;

namespace rentario_api.Models
{
    public partial class Paises
    {
        public Paises()
        {
            DivisionPais = new HashSet<DivisionPais>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }

        public virtual ICollection<DivisionPais> DivisionPais { get; set; }
    }
}
