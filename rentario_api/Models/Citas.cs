﻿using System;
using System.Collections.Generic;

namespace rentario_api.Models
{
    public partial class Citas
    {
        public int Id { get; set; }
        public int IdVivienda { get; set; }
        public int IdCliente { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaCita { get; set; }
        public int Estado { get; set; }
        public string Comentario { get; set; }
        public string ComentarioArrendatario { get; set; }

        public virtual EstadoCita EstadoNavigation { get; set; }
        public virtual Usuarios IdClienteNavigation { get; set; }
        public virtual ViviendasParaAlquilar IdViviendaNavigation { get; set; }
    }
}
