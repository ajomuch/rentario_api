﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace rentario_api.Models
{
    public partial class VwViviendasParaAlquilar
    {
        [NotMapped]
        public List<FotosXVivienda> Fotos { get; set; }
        [NotMapped]
        public bool Favorita { get; set; }
    }
}
