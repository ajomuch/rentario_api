﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace rentario_api.Models
{
    public partial class rentarioContext : DbContext
    {
        public rentarioContext()
        {
        }

        public rentarioContext(DbContextOptions<rentarioContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Citas> Citas { get; set; }
        public virtual DbSet<DivisionPais> DivisionPais { get; set; }
        public virtual DbSet<EstadoCita> EstadoCita { get; set; }
        public virtual DbSet<Favoritas> Favoritas { get; set; }
        public virtual DbSet<FotosXVivienda> FotosXVivienda { get; set; }
        public virtual DbSet<Paises> Paises { get; set; }
        public virtual DbSet<PlanXUsuario> PlanXUsuario { get; set; }
        public virtual DbSet<Planes> Planes { get; set; }
        public virtual DbSet<PuntuacionPorArrendatario> PuntuacionPorArrendatario { get; set; }
        public virtual DbSet<PuntuacionPorVivienda> PuntuacionPorVivienda { get; set; }
        public virtual DbSet<SubdivisionPais> SubdivisionPais { get; set; }
        public virtual DbSet<TipoUsuarios> TipoUsuarios { get; set; }
        public virtual DbSet<TipoVivienda> TipoVivienda { get; set; }
        public virtual DbSet<Usuarios> Usuarios { get; set; }
        public virtual DbSet<ViviendasParaAlquilar> ViviendasParaAlquilar { get; set; }
        public virtual DbSet<VwCitas> VwCitas { get; set; }
        public virtual DbSet<VwPuntuacionPorVivienda> VwPuntuacionPorVivienda { get; set; }
        public virtual DbSet<VwViviendasParaAlquilar> VwViviendasParaAlquilar { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseNpgsql("Host=rentario.c2wqfqiwcscu.us-east-2.rds.amazonaws.com;Database=rentario;Username=ajomuch92;Password=AlfaJulietMike92");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Citas>(entity =>
            {
                entity.ToTable("citas");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Comentario)
                    .HasColumnName("comentario")
                    .HasMaxLength(250);

                entity.Property(e => e.ComentarioArrendatario)
                    .HasColumnName("comentario_arrendatario")
                    .HasMaxLength(250);

                entity.Property(e => e.Estado).HasColumnName("estado");

                entity.Property(e => e.FechaCita).HasColumnName("fecha_cita");

                entity.Property(e => e.FechaCreacion).HasColumnName("fecha_creacion");

                entity.Property(e => e.IdCliente).HasColumnName("id_cliente");

                entity.Property(e => e.IdVivienda).HasColumnName("id_vivienda");

                entity.HasOne(d => d.EstadoNavigation)
                    .WithMany(p => p.Citas)
                    .HasPrincipalKey(p => p.Id)
                    .HasForeignKey(d => d.Estado)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fki_estado_citas");

                entity.HasOne(d => d.IdClienteNavigation)
                    .WithMany(p => p.Citas)
                    .HasForeignKey(d => d.IdCliente)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("citas_id_cliente_usuarios_id");

                entity.HasOne(d => d.IdViviendaNavigation)
                    .WithMany(p => p.Citas)
                    .HasForeignKey(d => d.IdVivienda)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("citas_id_vivienda_viviendas_para_alquilar_id");
            });

            modelBuilder.Entity<DivisionPais>(entity =>
            {
                entity.ToTable("division_pais");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.IdPais).HasColumnName("id_pais");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("nombre")
                    .HasMaxLength(100);

                entity.HasOne(d => d.IdPaisNavigation)
                    .WithMany(p => p.DivisionPais)
                    .HasForeignKey(d => d.IdPais)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("division_pais_id_pais_paises_id");
            });

            modelBuilder.Entity<EstadoCita>(entity =>
            {
                entity.HasKey(e => new { e.Id, e.Estado })
                    .HasName("estado_cita_pkey");

                entity.ToTable("estado_cita");

                entity.HasIndex(e => e.Id)
                    .HasName("uc_id")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd()
                    .UseIdentityAlwaysColumn();

                entity.Property(e => e.Estado)
                    .HasColumnName("estado")
                    .HasMaxLength(100);

                entity.Property(e => e.Accion)
                    .HasColumnName("accion")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Favoritas>(entity =>
            {
                entity.ToTable("favoritas");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Fecha).HasColumnName("fecha");

                entity.Property(e => e.IdUsuario).HasColumnName("id_usuario");

                entity.Property(e => e.IdVivienda).HasColumnName("id_vivienda");

                entity.HasOne(d => d.IdUsuarioNavigation)
                    .WithMany(p => p.Favoritas)
                    .HasForeignKey(d => d.IdUsuario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("favoritas_id_usuario_usuarios_id");

                entity.HasOne(d => d.IdViviendaNavigation)
                    .WithMany(p => p.Favoritas)
                    .HasForeignKey(d => d.IdVivienda)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("favoritas_id_vivienda_viviendas_para_alquilar_id");
            });

            modelBuilder.Entity<FotosXVivienda>(entity =>
            {
                entity.ToTable("fotos_x_vivienda");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.IdVivienda).HasColumnName("id_vivienda");

                entity.Property(e => e.Url)
                    .IsRequired()
                    .HasColumnName("url")
                    .HasMaxLength(250);

                entity.HasOne(d => d.IdViviendaNavigation)
                    .WithMany(p => p.FotosXVivienda)
                    .HasForeignKey(d => d.IdVivienda)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fotos_x_vivienda_id_vivienda_viviendas_para_alquilar_id");
            });

            modelBuilder.Entity<Paises>(entity =>
            {
                entity.ToTable("paises");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("nombre")
                    .HasColumnType("character varying");
            });

            modelBuilder.Entity<PlanXUsuario>(entity =>
            {
                entity.ToTable("plan_x_usuario");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Actual).HasColumnName("actual");

                entity.Property(e => e.FechaContratacion).HasColumnName("fecha_contratacion");

                entity.Property(e => e.IdPlan).HasColumnName("id_plan");

                entity.Property(e => e.IdUsuario).HasColumnName("id_usuario");

                entity.HasOne(d => d.IdPlanNavigation)
                    .WithMany(p => p.PlanXUsuario)
                    .HasForeignKey(d => d.IdPlan)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("plan_x_usuario_id_plan_planes_id");

                entity.HasOne(d => d.IdUsuarioNavigation)
                    .WithMany(p => p.PlanXUsuario)
                    .HasForeignKey(d => d.IdUsuario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("plan_x_usuario_id_usuario_usuarios_id");
            });

            modelBuilder.Entity<Planes>(entity =>
            {
                entity.ToTable("planes");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.DuracionDias).HasColumnName("duracion_dias");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("nombre")
                    .HasMaxLength(100);

                entity.Property(e => e.Precio).HasColumnName("precio");
            });

            modelBuilder.Entity<PuntuacionPorArrendatario>(entity =>
            {
                entity.ToTable("puntuacion_por_arrendatario");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Comentario)
                    .IsRequired()
                    .HasColumnName("comentario")
                    .HasMaxLength(250);

                entity.Property(e => e.Fecha).HasColumnName("fecha");

                entity.Property(e => e.IdArrendatario).HasColumnName("id_arrendatario");

                entity.Property(e => e.IdCliente).HasColumnName("id_cliente");

                entity.Property(e => e.Puntuacion).HasColumnName("puntuacion");

                entity.HasOne(d => d.IdArrendatarioNavigation)
                    .WithMany(p => p.PuntuacionPorArrendatarioIdArrendatarioNavigation)
                    .HasForeignKey(d => d.IdArrendatario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("puntuacion_por_arrendatario_id_arrendatario_usuarios_id");

                entity.HasOne(d => d.IdClienteNavigation)
                    .WithMany(p => p.PuntuacionPorArrendatarioIdClienteNavigation)
                    .HasForeignKey(d => d.IdCliente)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("puntuacion_por_arrendatario_id_cliente_usuarios_id");
            });

            modelBuilder.Entity<PuntuacionPorVivienda>(entity =>
            {
                entity.ToTable("puntuacion_por_vivienda");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Comentario)
                    .HasColumnName("comentario")
                    .HasMaxLength(250);

                entity.Property(e => e.Fecha).HasColumnName("fecha");

                entity.Property(e => e.IdUsuario).HasColumnName("id_usuario");

                entity.Property(e => e.IdVivienda).HasColumnName("id_vivienda");

                entity.Property(e => e.Puntuacion).HasColumnName("puntuacion");

                entity.HasOne(d => d.IdUsuarioNavigation)
                    .WithMany(p => p.PuntuacionPorVivienda)
                    .HasForeignKey(d => d.IdUsuario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("puntuacion_por_vivienda_id_usuario_usuarios_id");

                entity.HasOne(d => d.IdViviendaNavigation)
                    .WithMany(p => p.PuntuacionPorVivienda)
                    .HasForeignKey(d => d.IdVivienda)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("puntuacion_por_vivienda_id_vivienda_viviendas_para_alquilar_id");
            });

            modelBuilder.Entity<SubdivisionPais>(entity =>
            {
                entity.ToTable("subdivision_pais");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.IdDivision).HasColumnName("id_division");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("nombre")
                    .HasMaxLength(100);

                entity.HasOne(d => d.IdDivisionNavigation)
                    .WithMany(p => p.SubdivisionPais)
                    .HasForeignKey(d => d.IdDivision)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("subdivision_pais_id_subdivision_division_pais_id");
            });

            modelBuilder.Entity<TipoUsuarios>(entity =>
            {
                entity.ToTable("tipo_usuarios");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Estado).HasColumnName("estado");

                entity.Property(e => e.TipoUsuario)
                    .IsRequired()
                    .HasColumnName("tipo_usuario")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TipoVivienda>(entity =>
            {
                entity.ToTable("tipo_vivienda");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .UseIdentityAlwaysColumn();

                entity.Property(e => e.TipoVivienda1)
                    .IsRequired()
                    .HasColumnName("tipo_vivienda")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Usuarios>(entity =>
            {
                entity.ToTable("usuarios");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CodigoRecuperacion)
                    .HasColumnName("codigo_recuperacion")
                    .HasMaxLength(8);

                entity.Property(e => e.ContactarPorWhatsapp).HasColumnName("contactar_por_whatsapp");

                entity.Property(e => e.Contrasenia)
                    .IsRequired()
                    .HasColumnName("contrasenia")
                    .HasMaxLength(250);

                entity.Property(e => e.Correo)
                    .IsRequired()
                    .HasColumnName("correo")
                    .HasMaxLength(30);

                entity.Property(e => e.FcmToken)
                    .HasColumnName("fcm_token")
                    .HasMaxLength(1024);

                entity.Property(e => e.FechaCreacion).HasColumnName("fecha_creacion");

                entity.Property(e => e.FechaNacimiento)
                    .HasColumnName("fecha_nacimiento")
                    .HasColumnType("date");

                entity.Property(e => e.Genero)
                    .HasColumnName("genero")
                    .HasMaxLength(1);

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("nombre")
                    .HasMaxLength(100);

                entity.Property(e => e.PrimeraVez).HasColumnName("primera_vez");

                entity.Property(e => e.Telefono)
                    .HasColumnName("telefono")
                    .HasMaxLength(15);

                entity.Property(e => e.TipoUsuario).HasColumnName("tipo_usuario");

                entity.Property(e => e.UrlPerfil)
                    .HasColumnName("url_perfil")
                    .HasMaxLength(500);

                entity.HasOne(d => d.TipoUsuarioNavigation)
                    .WithMany(p => p.Usuarios)
                    .HasForeignKey(d => d.TipoUsuario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("usuarios_tipo_usuario_tipo_usuarios_id");
            });

            modelBuilder.Entity<ViviendasParaAlquilar>(entity =>
            {
                entity.ToTable("viviendas_para_alquilar");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Colonia)
                    .IsRequired()
                    .HasColumnName("colonia")
                    .HasMaxLength(50);

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnName("descripcion")
                    .HasMaxLength(500);

                entity.Property(e => e.DireccionExacta).HasColumnName("direccion_exacta");

                entity.Property(e => e.Dormitorios).HasColumnName("dormitorios");

                entity.Property(e => e.Estado).HasColumnName("estado");

                entity.Property(e => e.FechaPublicacion).HasColumnName("fecha_publicacion");

                entity.Property(e => e.IdSubdivision).HasColumnName("id_subdivision");

                entity.Property(e => e.IdTipoVivienda).HasColumnName("id_tipo_vivienda");

                entity.Property(e => e.IdUsuario).HasColumnName("id_usuario");

                entity.Property(e => e.Latitud).HasColumnName("latitud");

                entity.Property(e => e.Longitud).HasColumnName("longitud");

                entity.Property(e => e.Precio).HasColumnName("precio");

                entity.HasOne(d => d.IdSubdivisionNavigation)
                    .WithMany(p => p.ViviendasParaAlquilar)
                    .HasForeignKey(d => d.IdSubdivision)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("viviendas_para_alquilar_id_subdivision_subdivision_pais_id");

                entity.HasOne(d => d.IdTipoViviendaNavigation)
                    .WithMany(p => p.ViviendasParaAlquilar)
                    .HasForeignKey(d => d.IdTipoVivienda)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_vivienda_para_alquilar_tipo_vivienda");

                entity.HasOne(d => d.IdUsuarioNavigation)
                    .WithMany(p => p.ViviendasParaAlquilar)
                    .HasForeignKey(d => d.IdUsuario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("viviendas_para_alquilar_id_usuario_usuarios_id");
            });

            modelBuilder.Entity<VwCitas>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vw_citas");

                entity.Property(e => e.Cliente)
                    .HasColumnName("cliente")
                    .HasMaxLength(100);

                entity.Property(e => e.Comentario)
                    .HasColumnName("comentario")
                    .HasMaxLength(250);

                entity.Property(e => e.ComentarioArrendatario)
                    .HasColumnName("comentario_arrendatario")
                    .HasMaxLength(250);

                entity.Property(e => e.ContactarPorWhatsapp).HasColumnName("contactar_por_whatsapp");

                entity.Property(e => e.Estado).HasColumnName("estado");

                entity.Property(e => e.EstadoCita)
                    .HasColumnName("estado_cita")
                    .HasMaxLength(100);

                entity.Property(e => e.FechaCita).HasColumnName("fecha_cita");

                entity.Property(e => e.FechaCreacion).HasColumnName("fecha_creacion");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.IdCliente).HasColumnName("id_cliente");

                entity.Property(e => e.IdVivienda).HasColumnName("id_vivienda");

                entity.Property(e => e.Telefono)
                    .HasColumnName("telefono")
                    .HasMaxLength(15);

                entity.Property(e => e.UrlPerfil)
                    .HasColumnName("url_perfil")
                    .HasMaxLength(500);
            });

            modelBuilder.Entity<VwPuntuacionPorVivienda>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vw_puntuacion_por_vivienda");

                entity.Property(e => e.Comentario)
                    .HasColumnName("comentario")
                    .HasMaxLength(250);

                entity.Property(e => e.Fecha).HasColumnName("fecha");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.IdUsuario).HasColumnName("id_usuario");

                entity.Property(e => e.IdVivienda).HasColumnName("id_vivienda");

                entity.Property(e => e.Nombreusuario)
                    .HasColumnName("nombreusuario")
                    .HasMaxLength(100);

                entity.Property(e => e.Puntuacion).HasColumnName("puntuacion");

                entity.Property(e => e.UrlPerfil)
                    .HasColumnName("url_perfil")
                    .HasMaxLength(500);
            });

            modelBuilder.Entity<VwViviendasParaAlquilar>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vw_viviendas_para_alquilar");

                entity.Property(e => e.Arrendatario)
                    .HasColumnName("arrendatario")
                    .HasMaxLength(100);

                entity.Property(e => e.CantidadComentario).HasColumnName("cantidad_comentario");

                entity.Property(e => e.Colonia)
                    .HasColumnName("colonia")
                    .HasMaxLength(50);

                entity.Property(e => e.ContactarPorWhatsapp).HasColumnName("contactar_por_whatsapp");

                entity.Property(e => e.Descripcion)
                    .HasColumnName("descripcion")
                    .HasMaxLength(500);

                entity.Property(e => e.DireccionExacta).HasColumnName("direccion_exacta");

                entity.Property(e => e.Dormitorios).HasColumnName("dormitorios");

                entity.Property(e => e.Estado).HasColumnName("estado");

                entity.Property(e => e.FechaPublicacion).HasColumnName("fecha_publicacion");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.IdDivision).HasColumnName("id_division");

                entity.Property(e => e.IdPais).HasColumnName("id_pais");

                entity.Property(e => e.IdSubdivision).HasColumnName("id_subdivision");

                entity.Property(e => e.IdTipoVivienda).HasColumnName("id_tipo_vivienda");

                entity.Property(e => e.IdUsuario).HasColumnName("id_usuario");

                entity.Property(e => e.Latitud).HasColumnName("latitud");

                entity.Property(e => e.Longitud).HasColumnName("longitud");

                entity.Property(e => e.Lugar)
                    .HasColumnName("lugar")
                    .HasColumnType("character varying");

                entity.Property(e => e.Precio).HasColumnName("precio");

                entity.Property(e => e.PromedioPuntuacion).HasColumnName("promedio_puntuacion");

                entity.Property(e => e.Telefono)
                    .HasColumnName("telefono")
                    .HasMaxLength(15);

                entity.Property(e => e.TipoVivienda)
                    .HasColumnName("tipo_vivienda")
                    .HasMaxLength(50);

                entity.Property(e => e.UrlPerfil)
                    .HasColumnName("url_perfil")
                    .HasMaxLength(500);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
