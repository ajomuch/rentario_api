﻿using System;
using System.Collections.Generic;

namespace rentario_api.Models
{
    public partial class Usuarios
    {
        public Usuarios()
        {
            Citas = new HashSet<Citas>();
            Favoritas = new HashSet<Favoritas>();
            PlanXUsuario = new HashSet<PlanXUsuario>();
            PuntuacionPorArrendatarioIdArrendatarioNavigation = new HashSet<PuntuacionPorArrendatario>();
            PuntuacionPorArrendatarioIdClienteNavigation = new HashSet<PuntuacionPorArrendatario>();
            PuntuacionPorVivienda = new HashSet<PuntuacionPorVivienda>();
            ViviendasParaAlquilar = new HashSet<ViviendasParaAlquilar>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Correo { get; set; }
        public string Telefono { get; set; }
        public string Contrasenia { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string UrlPerfil { get; set; }
        public int TipoUsuario { get; set; }
        public string Genero { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public bool? PrimeraVez { get; set; }
        public string FcmToken { get; set; }
        public bool? ContactarPorWhatsapp { get; set; }
        public string CodigoRecuperacion { get; set; }

        public virtual TipoUsuarios TipoUsuarioNavigation { get; set; }
        public virtual ICollection<Citas> Citas { get; set; }
        public virtual ICollection<Favoritas> Favoritas { get; set; }
        public virtual ICollection<PlanXUsuario> PlanXUsuario { get; set; }
        public virtual ICollection<PuntuacionPorArrendatario> PuntuacionPorArrendatarioIdArrendatarioNavigation { get; set; }
        public virtual ICollection<PuntuacionPorArrendatario> PuntuacionPorArrendatarioIdClienteNavigation { get; set; }
        public virtual ICollection<PuntuacionPorVivienda> PuntuacionPorVivienda { get; set; }
        public virtual ICollection<ViviendasParaAlquilar> ViviendasParaAlquilar { get; set; }
    }
}
