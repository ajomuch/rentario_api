﻿using System;
using System.Collections.Generic;

namespace rentario_api.Models
{
    public partial class VwViviendasParaAlquilar
    {
        public int? Id { get; set; }
        public int? IdTipoVivienda { get; set; }
        public string TipoVivienda { get; set; }
        public string Colonia { get; set; }
        public int? DireccionExacta { get; set; }
        public float? Latitud { get; set; }
        public float? Longitud { get; set; }
        public string Descripcion { get; set; }
        public int? Dormitorios { get; set; }
        public bool? Estado { get; set; }
        public float? Precio { get; set; }
        public string Arrendatario { get; set; }
        public int? CantidadComentario { get; set; }
        public float? PromedioPuntuacion { get; set; }
        public string Lugar { get; set; }
        public int? IdSubdivision { get; set; }
        public int? IdDivision { get; set; }
        public int? IdPais { get; set; }
        public DateTime? FechaPublicacion { get; set; }
        public int? IdUsuario { get; set; }
        public string UrlPerfil { get; set; }
        public string Telefono { get; set; }
        public bool? ContactarPorWhatsapp { get; set; }
    }
}
