﻿using CorePush.Google;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace rentario_api.Utils
{
    public class PushNotification
    {
        private readonly HttpClient httpClient = new HttpClient();
        public async Task<bool> sendNotification(String ServerKey, string SenderId, String deviceToken, object notification)
        {
            try
            {
                FcmSettings settings = new FcmSettings();
                settings.ServerKey = ServerKey;
                settings.SenderId = SenderId;
                FcmSender fcm = new FcmSender(settings, httpClient);
                FcmResponse response = await fcm.SendAsync(deviceToken, notification);
                return response.Success != 0;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
