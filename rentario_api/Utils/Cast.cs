﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rentario_api.Utils
{
    public static class Cast
    {
        public static bool TryCast<T>(this object obj, out T result)
        {
            try
            {
                result = JsonConvert.DeserializeObject<T>(obj.ToString());
                return true;
            }
            catch(Exception ex)
            {
                result = default(T);
                return false;
            }
        }
    }
}
